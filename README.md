# CyberPetrovna.Art

Repository for visual arts related to CyberPetrovna universe and characters. Entries welcome.

## Contributing

 * No individual licenses per file planned (you can discuss this in Issues). License for all files is GPLv3.
(so no files with incompatible initial licenses, please)

 * Using source files (for Gimp/Krita/Photoshop etc.) allows for easier modification.

 * Proprietary image editors inhibit modification and are discouraged.

 * Art you submit is treated as a part of the project and may be modified.
 (although probably we will backup originals somewhere).